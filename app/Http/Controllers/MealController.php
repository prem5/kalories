<?php

namespace App\Http\Controllers;

use App\Http\Requests\MealRequest;
use App\Meal;
use App\Repositories\MealRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MealController extends Controller
{
    protected $model;

    public function __construct(Meal $meal)
    {
        $this->model = new MealRepository($meal);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meals = $this->model->filter(\request('range'));
        return view('meal.index',compact('meals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MealRequest $request)
    {
        $this->model->create($request->all());
        Session::flash('success_message', "Meal Created successfully!.");
        return redirect('meals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meal =  $this->model->show($id);
        return view('meal.show',compact('meal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $meal =  $this->model->show($id);
       return view('meal.edit',compact('meal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MealRequest $request, $id)
    {
        // update model and only pass in the fillable fields
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        Session::flash('success_message', "Meal Updated successfully!.");
        return redirect('meals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->delete($id);
        Session::flash('success_message', "Meal Deleted successfully!.");
        return redirect('meals');
    }

}
