<?php
/**
 * Created by PhpStorm.
 * User: prem
 * Date: 10/27/18
 * Time: 10:30 PM
 */

namespace App\Repositories;


use App\Meal;
use Illuminate\Support\Facades\Auth;

class MealRepository implements RepositoryInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Meal $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->where('user_id',Auth::user()->id)->get();
    }

    // create a new record in the database
    public function create(array $data)
    {
        $data = $this->prepareData($data);
        $data['user_id'] = Auth::user()->id;
        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->show($id);
        $data = $this->prepareData($data);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->where('user_id',Auth::user()->id)->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function prepareData($data)
    {
        $data['meal_date'] = date('Y-m-d',strtotime($data['meal_date']));
        $data['meal_time'] = date('Y-m-d H:i:s',strtotime($data['meal_time']));
        return $data;
    }

    public function filter()
    {
        if(request('start_date')){
            $fromdate = date('Y-m-d',strtotime(request('start_date'))) ;
            $todate = date('Y-m-d',strtotime(request('end_date'))) ;
            return $this->model->where('user_id',Auth::user()->id)->whereBetween('meal_date', array($fromdate,$todate))->orderBy('id','DESC')->get();
        }else{
            return $this->all();
        }
    }
}