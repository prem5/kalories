<?php
/**
 * Created by PhpStorm.
 * User: prem
 * Date: 10/27/18
 * Time: 9:59 PM
 */

namespace App\Repositories;


interface RepositoryInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}