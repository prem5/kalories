@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="pull-left">Meal Detail</span>
                        <a href="{{url('meals')}}" class="btn btn-primary pull-right">List </a>
                    </div>

                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-lg-4 pull-left">
                               <span>Date:</span>
                            </div>
                            <div class="col-lg-8 pull-right">
                               {{$meal->meal_date}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4 pull-left">
                                <span>Time:</span>
                            </div>
                            <div class="col-lg-8 pull-right">
                                {{$meal->meal_time}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4 pull-left">
                                <span>Description:</span>
                            </div>
                            <div class="col-lg-8 pull-right">
                                {!! $meal->description !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4 pull-left">
                                <span>Calories:</span>
                            </div>
                            <div class="col-lg-8 pull-right">
                                {!! $meal->calories !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10">
                                <a href="{{url('meals')}}" class="btn btn-secondary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
