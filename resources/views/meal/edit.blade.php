@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="pull-left">Edit Meal</span>
                        <a href="{{url('meals')}}" class="btn btn-primary pull-right">List </a>
                    </div>

                    <div class="card-body">
                        {!! Form::model($meal, ['route' => ['meals.update', $meal->id], 'method' => 'PUT']) !!}

                        @include('meal.form')
                        <div class="form-group">
                            <div class="col-lg-10">
                                {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
