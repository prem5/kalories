@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-10">
                @include('include.message')
                <div class="card">
                    <div class="card-header">
                        <span class="pull-left">Meals</span>
                        <a href="{{url('meals/create')}}" class="btn btn-primary pull-right">Add New</a>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['url'=>'meals','method'=>'GET','id'=>'myform']) !!}
                        <div class="form-group row">
                            <div class="col-md-3 ">
                                <span>Date Range:</span>
                            </div>
                            <div class="col-md-3 ">
                                <input id="start_date" name="start_date" type="text" class=" form-control " value="{{request('start_date')}}">
                            </div>
                            <div class="col-md-3 ">
                                <input id="end_date" name="end_date" type="text" class=" form-control " value="{{request('end_date')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 ">
                                <button onclick="clearForm()"  class=" form-control " value="Clear">Clear</button>
                            </div>
                            <div class="col-md-3 ">
                                <input  type="submit" class=" form-control " value="Filter">
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="table-responsive data-table-wrapper">
                            <table id="blogs-table" class="table table-condensed table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Calories</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <thead class="transparent-bg">
                                @php $total = 0; @endphp
                                @foreach($meals as $meal)
                                <tr  class="@if($meal->meal_date == date('Y-m-d'))today @else otherday @endif">
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$meal->meal_date}}</td>
                                    <td>{{$meal->meal_time}}</td>
                                    <td>{{$meal->calories}}</td>
                                    <td>
                                        <a href="{{url('meals/'.$meal->id)}}">Show</a>
                                        <a href="{{url('meals/'.$meal->id."/edit")}}">Edit</a>

                                        {!! Form::open([
                                     'method'=>'DELETE',
                                     'url' => ['meals', $meal->id],
                                     'style' => 'display:inline'
                                 ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete " >Delete</span>', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                    @php $total += $meal->calories @endphp
                                @endforeach
                                <tr  >
                                    <td colspan="3"></td>
                                    <td>Total : {{$total}}</td>
                                    <td>

                                    </td>
                                </tr>
                                </thead>
                            </table>
                        </div><!--table-responsive-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

