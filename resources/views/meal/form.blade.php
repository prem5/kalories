<div class="form-group">
    {{ Form::label('meal_date', "Date", ['class' => 'col-lg-2 control-label required']) }}

    <div class="col-lg-10">
        {{ Form::text('meal_date', null, ['class' => 'form-control box-size datepicker', 'placeholder' => "Date", 'required' => 'required']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('meal_time', "Time", ['class' => 'col-lg-2 control-label required']) }}

    <div class="col-lg-10">
        {{ Form::text('meal_time', null, ['class' => 'form-control box-size timepicker', 'placeholder' => "Time", 'required' => 'required']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('description', "Detail", ['class' => 'col-lg-2 control-label required']) }}

    <div class="col-lg-10">
        {!! Form::textarea('description',null, ['class' => 'form-control','placeholder' => "Detail",'required'=>'required']) !!}
    </div>
</div>
<div class="form-group">
    {{ Form::label('calories', "Calories", ['class' => 'col-lg-2 control-label required']) }}

    <div class="col-lg-10">
        {{ Form::number('calories', null, ['class' => 'form-control box-size ', 'placeholder' => "Number of Calories", 'required' => 'required','min'=>0]) }}
    </div>
</div>